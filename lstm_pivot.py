import os
import re
from sklearn.feature_extraction.text import CountVectorizer
from collections import OrderedDict
import collections
import random

from sklearn.metrics import precision_recall_fscore_support
import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
# fix random seed for reproducibility
np.random.seed(8)

path = "/home/nimainitai/nlp/EnronData-v0.03/main-noheader"
# Read the data into a pandas dataframe called emails

msgs = dict()
labels = dict()
limit=10000
count=0
exit_flag=0
main_dirs = [o for o in os.listdir(path) if os.path.isdir(os.path.join(path,o))]
#main_dirs.remove('business')
#main_dirs.remove('personal')
labels=dict()
#print(dirs)
for i in range(0, len(main_dirs)):
    for root_path,dirs,files in os.walk(os.path.join(path,main_dirs[i])):
        for x in files:
            flag=0
            labels[x]= main_dirs[i]
            with open(os.path.join(root_path,x), 'r', encoding='utf-8') as f:
                name = x
                msg = str(f.read())
                msgs[name]= msg
            count+=1
            if count>=limit:
                exit_flag=1
                break
    if exit_flag:
            break

print(list(labels.values()).count('business'))

o_labels = collections.OrderedDict(random.sample(list(labels.items()),len(labels)))


top_words=5000
#(X_train, train_y), (X_test, test_y) = imdb.load_data(nb_words=top_words)
##selecting only relevant labels

msgs = OrderedDict(sorted(msgs.items()))
vectizer = CountVectorizer(max_features=5000)
vectizer.fit([i for i in list(msgs.values())])
analyzer = vectizer.build_analyzer()
mod_msgs = [[vectizer.vocabulary_.get(x) for x in analyzer(msg) if x in vectizer.vocabulary_] for msg in [msgs[w] for w in o_labels]]
train_len = int(len(msgs)*0.8)
test_len = len(msgs) - train_len
X_train = mod_msgs[:train_len]
X_test = mod_msgs[train_len:]


train_y = [ main_dirs.index(o_labels[key]) for key in list(o_labels.keys())[:train_len]]
test_y = [ main_dirs.index(o_labels[key]) for key in list(o_labels.keys())[train_len:]]

#print (mod_msgs)
max_msg_length = 500
X_train = sequence.pad_sequences(X_train, maxlen = max_msg_length)
X_test = sequence.pad_sequences(X_test, maxlen = max_msg_length)

embedding_vecor_length = 32
model = Sequential()
model.add(Embedding(top_words, embedding_vecor_length, input_length=max_msg_length))
model.add(LSTM(100))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#print(model.summary())
model.fit(X_train, train_y, nb_epoch=3, batch_size=64)
# Final evaluation of the model
#print('-------Predictions-------')
predictions = model.predict_classes(X_test,batch_size=64)
predictions = [i for j in predictions for i in j]
print(precision_recall_fscore_support(test_y,predictions))
scores = model.evaluate(X_test, test_y, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))
