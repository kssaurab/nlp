import os
import pandas as pd
import numpy as np
import nltk.data 
from nltk.tokenize import RegexpTokenizer,WordPunctTokenizer
import re
import collections
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron  
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import gensim
from IPython.core.debugger import Tracer
import pdb
import random
from nltk.corpus import stopwords
from sklearn.metrics import precision_recall_fscore_support
path = "/home/nimainitai/nlp/EnronData-v0.03/main-noheader"
# Read the data into a pandas dataframe called emails

def tok(msgs):
    toks = dict()
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    email = re.compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")
    tokenizer = RegexpTokenizer(r"\w+")
#    tokenizer = WordPunctTokenizer()
    for key in msgs:
        toks[key] = [tokenizer.tokenize(j) for j in sent_detector.tokenize(msgs[key].strip())]
    return toks


def train(dim,data,window_size=5):
    o_labels = data[0]
    toks = data[1]
    train_len = data[2]
    test_len = data[3]
    train_sentences = [ sent for key in list(o_labels.keys())[:train_len] for sent in toks[key]]
    model = gensim.models.Word2Vec(train_sentences, size=dim, window=window_size, min_count=5, workers=50)
    vecAvg = []
    for i in range(0,train_len):
        vecAvg.append(np.mean( np.array([model[word] for key in list(o_labels.keys())[:train_len] for sent in toks[key] for word in sent if word in model.vocab] ), axis=0))
    #print(len(vecAvg))
    test_vecAvg = []
    for i in range(0,test_len):
        test_vecAvg.append(np.mean( np.array([model[word] for key in list(o_labels.keys())[train_len:] for sent in toks[key] for word in sent if word in model.vocab] ), axis=0))
    return vecAvg,test_vecAvg


msgs = dict()
labels = dict()
limit=10000
count=0
exit_flag=0
main_dirs = [o for o in os.listdir(path) if os.path.isdir(os.path.join(path,o))]
main_dirs.remove('business')
main_dirs.remove('personal')
labels=dict()
#print(dirs)
for i in range(0, len(main_dirs)):
    for root_path,dirs,files in os.walk(os.path.join(path,main_dirs[i])):
        for x in files:
            flag=0
            labels[x]= main_dirs[i]
            with open(os.path.join(root_path,x), 'r', encoding='utf-8') as f:
                name = x
                msg = str(f.read)
                msgs[name]= msg
            count+=1
            if count>=limit:
                exit_flag=1
                break
    if exit_flag:
            break

print(list(labels.values()).count('business'))

toks = tok(msgs)
o_labels = collections.OrderedDict(random.sample(list(labels.items()),len(labels)))

sents = []
num_sents=[]
totalsents = 0
#print(len(msgs))
train_len = int(len(toks)*0.8)
test_len = len(toks) - train_len
data = [o_labels,toks,train_len,test_len]
vecAvg, test_vecAvg =  train(dim=10,data=data)

train_y = [ o_labels[key] for key in list(o_labels.keys())[:train_len]]
test_y = [ o_labels[key] for key in list(o_labels.keys())[train_len:]]
#clf = LogisticRegression(multi_class='multinomial',solver='newton-cg')
clf = GaussianNB()
clf.fit(vecAvg,train_y)
predictions = clf.predict(test_vecAvg)
accur = accuracy_score(test_y, predictions)
print(precision_recall_fscore_support(test_y,predictions))
#print('------------test--------\n',test_y)
print('---------------predictions\n',predictions)
