import os
import re
from sklearn.feature_extraction.text import CountVectorizer
from collections import OrderedDict


import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
# fix random seed for reproducibility
np.random.seed(7)
path = "/home/nimainitai/nlp/enron_with_categories/1"
# Read the data into a pandas dataframe called emails

a=[]
msgs = dict()
labels = dict()
limit=5000
count=0
txt = re.compile(r"\d+\.txt")
#cats = re.compile(r"\d+\.cats")
exit_flag=0
for path, dirs, files in os.walk(path):
    for x in files:
        flag=0
        if txt.match(x):
            print(x)
            with open(os.path.join(path,x), 'r', encoding='utf-8') as f:
                name = x[:-4]
                line = f.readlines()             
                msgs[name]=''
                for sent in line:
                    if flag==1:
                        msgs[name]+= sent
                    if sent.startswith('X-FileName:'):
                        flag=1
            labels[name]=[]
            with open(os.path.join(path,name+'.cats'),'r') as f:
                labels[name] += f.readlines()
            count+=1
            if count>=limit:
                exit_flag=1
                break
    if exit_flag:
        break

top_words=5000
#(X_train, y_train), (X_test, y_test) = imdb.load_data(nb_words=top_words)
##selecting only relevant labels
modified_labels = []
for key in list(labels.keys()):
    label_vect = [0]*14
    for line in labels[key]:
        temp = list(map(int ,(line.strip()).split(',')))
        if temp[0]==3:
            label_vect[temp[1]]= 1
    modified_labels.append(np.array(label_vect))
mod_lab_transpose = np.transpose(np.vstack(modified_labels))
msgs = OrderedDict(sorted(msgs.items()))
vectizer = CountVectorizer(max_features=5000)
vectizer.fit([i for i in list(msgs.values())])
analyzer = vectizer.build_analyzer()
mod_msgs = [[vectizer.vocabulary_.get(x) for x in analyzer(msg) if x in vectizer.vocabulary_] for msg in list(msgs.values())]
train_len = int(len(msgs)*0.8)
test_len = len(msgs) - train_len
temp = [i for i in mod_msgs]
X_train = temp[:train_len]
X_test = temp[train_len:]
y_train = mod_lab_transpose[1][:train_len]
#print(y_train)
y_test = mod_lab_transpose[1][train_len:]

#print (mod_msgs)
max_msg_length = 500
X_train = sequence.pad_sequences(X_train, maxlen = max_msg_length)
X_test = sequence.pad_sequences(X_test, maxlen = max_msg_length)

embedding_vecor_length = 32
model = Sequential()
model.add(Embedding(top_words, embedding_vecor_length, input_length=max_msg_length))
model.add(LSTM(100))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#print(model.summary())
model.fit(X_train, y_train, nb_epoch=3, batch_size=64)
# Final evaluation of the model
print('-------Predictions-------')
print(model.predict_classes(X_test,batch_size=64))
print('-----------Test-------')
print(y_test)
scores = model.evaluate(X_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))
