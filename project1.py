
# coding: utf-8

# In[3]:

import os
import pandas as pd
pd.set_option("display.max_rows", 999)
pd.set_option('max_colwidth',100)
import numpy as np
import nltk.data 
from nltk.tokenize import RegexpTokenizer,WordPunctTokenizer
import re
import collections
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron  
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score
import gensim
from IPython.core.debugger import Tracer
import pdb

from nltk.corpus import stopwords


path = "/home/nimainitai/nlp/enron_with_categories/1"
# Read the data into a pandas dataframe called emails

a=[]
msgs = dict()
labels = dict()
limit=200
count=0
txt = re.compile(r"\d+\.txt")
#cats = re.compile(r"\d+\.cats")
exit_flag=0
for path, dirs, files in os.walk(path):
    for x in files:
        flag=0
        if txt.match(x):
            print(x)
            with open(os.path.join(path,x), 'r', encoding='utf-8') as f:
                name = x[:-4]
                line = f.readlines()             
                msgs[name]=''
                for sent in line:
                    if flag==1:
                        msgs[name]+= sent#' '.join([word for word in sent.split() if word not in (stopwords.words('english'))])
                    if sent.startswith('X-FileName:'):
                        flag=1
            labels[name]=[]
            with open(os.path.join(path,name+'.cats'),'r') as f:
                labels[name] += f.readlines()
            count+=1
            if count>=limit:
                exit_flag=1
                break
    if exit_flag:
        break

#for d in msgs:
#    print (d+':'+msgs[d]+'\n>>>>>>>>>>>>>>>>>>\n')



# In[4]:

def tok(msgs):
    toks = dict()
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    email = re.compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")
    tokenizer = RegexpTokenizer(r"\w+")
#    tokenizer = WordPunctTokenizer()
    for key in msgs:
        
        msgs[key] = email.sub("8email8",msgs[key])
        #print (temp)
        toks[key] = [tokenizer.tokenize(j) for j in sent_detector.tokenize(msgs[key].strip())]
    return toks

toks = tok(msgs)
toks = collections.OrderedDict(sorted(toks.items()))
labels = collections.OrderedDict(sorted(labels.items()))
sents = []
num_sents=[]
totalsents = 0
#print(len(msgs))
for i in toks:
    totalsents+=len(toks[i])
    num_sents.append(totalsents)
    sents+=toks[i]
        


# In[5]:

def train(dim,data,window_size=5):
    train_num_sents = data[0]
    train_index = data[1]
    train_sentences = data[2]
    test_num_sents = data[3]
    test_index = data[4]
    test_sentences = data[5]
    model = gensim.models.Word2Vec(train_sentences, size=dim, window=window_size, min_count=5, workers=50)
        #print(model['ago.'])
#    print (train_sentences)
    sents_over = 0
    vecAvg = []
    for i in range(0,train_index):
        vecAvg.append(np.mean( np.array([model[word] for sentence in train_sentences[sents_over:train_num_sents[i]] for word in sentence if word in model.vocab] ), axis=0))
        sents_over=train_num_sents[i]
    #print(len(vecAvg))
    sents_over = 0
    test_vecAvg = []
    for i in range(0,test_index):
        test_vecAvg.append(np.mean( np.array([model[word] for sentence in test_sentences[sents_over:test_num_sents[i]] for word in sentence if word in model.vocab]), axis=0))
        sents_over=test_num_sents[i]
    return vecAvg,test_vecAvg



# In[6]:

train_count = int(len(num_sents)*0.8)
test_count = len(num_sents)-train_count
print(test_count)
test_num_sents=[]
for i in range(0,test_count):
    test_num_sents.append(num_sents[train_count+i]-num_sents[train_count-1])
data = [num_sents[:train_count],train_count,sents[:num_sents[train_count-1]],test_num_sents,test_count,sents[num_sents[train_count-1]:]]
vecAvg, test_vecAvg = train(dim=20,data=data)

modified_labels = []
for key in list(labels.keys()):
    label_vect = [0]*14
    for line in labels[key]:
        temp = list(map(int ,(line.strip()).split(',')))
        if temp[0]==3:
            label_vect[temp[1]]=1
    modified_labels.append(label_vect)
#print(toks.keys())
#train_y = [np.true_divide(i,np.sum(i)) if i!=[0]*13 else i for i in modified_labels[:train_count]]
#test_y = [np.true_divide(i,np.sum(i)) if i!=[0]*13 else i for i in modified_labels[train_count:]]
train_y = [np.array(i) for i in modified_labels[:train_count]]
test_y = [np.array(i) for i in modified_labels[train_count:]]
#print(train_y)
# In[ ]:

clf = SVC()
#clf = LogisticRegression()
try:
    clf.fit(vecAvg,train_y)
except Exception as err:
    pass
predictions=[None]*14
for i in range(0,14):
    temp = [train_y[j][i] for j in range(0,len(train_y))]
    if temp.count(0) == len(temp):
        predictions[i]=np.zeros(test_count,dtype=np.float64)
        continue
    clf.fit(vecAvg,temp)
    try:
        predictions[i] = np.array(clf.predict(test_vecAvg))
#        print(predictions[i].shape)
    except Exception as err:
        print('-------TestVecAvg---------')
        print(test_y[0])
        print('-------TestVecAvg---------')

print('--------predictions_y------\n',predictions[0])
total_tp=0
total_tn=0
total_fp=0
total_fn=0
for i in range(0,14):
    tp = 0
    tn=0
    fp=0
    fn=0
    for j in range(0,test_count):
        if test_y[j][i]==0:
            if predictions[i][j]<1:
                tn+=1
            else:
                fp+=1
        else:
            if predictions[i][j]>0:
                tp+=1
            else:
                fn+=1
    total_tp+=tp
    total_fp+=fp
    total_fn+=fn
    total_tn+=tn

print('macro_precision =' , float(total_tp)/(total_tp+total_fp))


